#version 330

uniform sampler2D texture_sampler;
uniform sampler2D nmap_sampler;

struct LightInfo
{
    vec3 pos; 
    int point_flag;
    vec3 La, Ld, Ls;
};
uniform LightInfo light_info;

struct MaterialInfo
{
    vec3 Ka, Kd, Ks;
    float shininess;
};
uniform MaterialInfo material_info;
uniform vec3 camera_pos;

uniform int with_texture;
uniform int with_normal_map;

// работаем в СК вселенной
in vec2 tex_coords;
in vec3 normal;
in vec3 position;
in mat3 normal_rotate;

out vec4 fragColor;



void main()
{
    vec3 to_light_dir;
    if (light_info.point_flag == 1){
        to_light_dir = normalize(light_info.pos - position);
    } else {
        to_light_dir = -normalize(light_info.pos);
    }
    vec3 n = normalize(normal);
    if (with_normal_map == 1){
        n = normal_rotate * normalize((2 * texture(nmap_sampler, tex_coords).xyz - vec3(1, 1, 1)));
    }
    float NdotL = max(0, dot(n, to_light_dir));
    fragColor = vec4(light_info.La * material_info.Ka, 1);
    fragColor += vec4(light_info.Ld * material_info.Kd * NdotL, 0);
    if (with_texture == 1){
        fragColor *= vec4(texture(texture_sampler, tex_coords).rgb, 1);
    }
    if (NdotL > 0) { // бликовое освещение классическое по Фонгу
        vec3 to_cam_dir = normalize(camera_pos - position);
        float CdotLR = max(0, dot(to_cam_dir, -reflect(to_light_dir, n)));
        fragColor += vec4(light_info.Ls * material_info.Ks * pow(CdotLR, material_info.shininess), 0);
    } 
}
