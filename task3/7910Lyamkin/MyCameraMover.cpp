#include "MyCameraMover.h"

#include "Camera.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>

void MyOrbitCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // do nothing
}

void MyOrbitCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        _phiAng -= dx * 0.0005;
        _thetaAng += dy * 0.0005;

        _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MyOrbitCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
    _r += _r * yoffset * 0.05;
}

void MyOrbitCameraMover::update(GLFWwindow* window, double dt)
{
    double speed = 1.0;

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _phiAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _phiAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _thetaAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _thetaAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        _r += _r * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
    {
        _r -= _r * dt;
    }

    _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);

    //-----------------------------------------

    //��������� ��������� ����������� ������ � ������� ������� ��������� �� ������� ����������� ���������
    glm::vec3 pos = glm::vec3(glm::cos(_phiAng) * glm::cos(_thetaAng), glm::sin(_phiAng) * glm::cos(_thetaAng), glm::sin(_thetaAng) + 0.5f) * (float)_r;

    //��������� ������� ����
    _camera.viewMatrix = glm::lookAt(_pos + pos, _pos, glm::vec3(0.0f, 0.0f, 1.0f));

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //��������� ������� �������� �� ������, ���� ������� ���� ����������
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

glm::vec3 MyOrbitCameraMover::position() const
{
    return _pos;
}

//=============================================

MyFreeCameraMover::MyFreeCameraMover() :
    CameraMover(),
    _pos(0.0f, 0.0f, 0.5f)
{
    //��� ����� ���-������ ��������� ��������� ���������� ������
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(1.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
    _near = NEAR;
}

void MyFreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // do nothing
}

void MyFreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    glfwSetCursorPos(window, _screenw / 2, _screenh / 2);
    // ����� ���� ������������� ������ � ������ ������ (�� �������)
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.001), glm::vec3(0.0f, 0.0f, 1.0f));
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.001), glm::vec3(1, 0, 0) * _rot);
}

void MyFreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
    // do nothing
}

void MyFreeCameraMover::update(GLFWwindow* window, double dt)
{
    glm::vec3 new_pos = _pos + _shift(window, dt);
    if (_map->canGo(_pos, new_pos)) { // ���������, ������ ���� �� ��������� � �����������
        _pos = new_pos;
        _updateMatrices();
        }
}

bool MyFreeCameraMover::teleport(GLFWwindow* window, double dt, Portal& portal)
{
    glm::vec3 new_pos = _pos + _shift(window, dt);
    bool teleported = portal.teleport(_pos, new_pos, _rot);
    if (teleported) {
        _pos = new_pos;
        _updateMatrices();
    }
    return teleported;
}

void MyFreeCameraMover::updateScreen(int w, int h)
{
    _screenw = w, _screenh = h;
    _oldXPos = _screenw / 2;
    _oldYPos = _screenh / 2;
}

float MyFreeCameraMover::OZ_angle()
{
    glm::vec3 ox_image = glm::vec3(0, 0, -1) * _rot;
    glm::vec2 image = glm::normalize(glm::vec2(ox_image.x, ox_image.y)), ox = glm::vec2(1, 0);
    float alpha = glm::acos(glm::dot(image, ox));
    if (image.y < 0)
        alpha = -alpha;
    return alpha;
}

glm::vec3 MyFreeCameraMover::position() const
{
    return _pos;
}

void MyFreeCameraMover::setMap(Map* map)
{
    _map = map;
}

glm::vec3 MyFreeCameraMover::_shift(GLFWwindow* window, double dt)
{
    //�������� ������� ����������� "������" � ������� ������� ���������
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
    forwDir.z = 0;

    //�������� ������� ����������� "������" � ������� ������� ���������
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
    rightDir.z = 0;

    //������� ������ ������/�����
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        return forwDir * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        return - forwDir * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        return - rightDir * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        return rightDir * static_cast<float>(dt);
    }
    return glm::vec3(0, 0, 0);
}

void MyFreeCameraMover::_updateMatrices()
{
    //��������� ����������� � ������� ������
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    _camera.projMatrix = glm::perspective(glm::radians(75.0f), (float)_screenw / _screenh, _near, _far); //��������� ������� �������� �� ������, ���� ������� ���� ����������

}
