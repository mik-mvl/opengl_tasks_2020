#pragma once
#include <iostream>

#include "utils.h"
#include "MyCameraMover.h"
#include "map.h"
#include "mesh.h"

std::pair<int, int> monitorSize();

class Application
{
public:
	Application();
	~Application();

	void run();

	void handleKey(int key, int scancode, int action, int mods);
	void handleMouseMove(double xpos, double ypos);
	void handleScroll(double xoffset, double yoffset);
private:
	void _setLight();
	void _setLightInfo(glm::vec3 pos, bool point, glm::vec3 La, glm::vec3 Ld, glm::vec3 Ls);
	void _make_window();
	void _changeCamera();
	
	void _initAll();
	void _initMeshes();
	void _setCallback(); // ��������� �������: �������� ������ ��� ��� ������������ ������
	void _makeScene();
	void _update();
	void _updateMesh();



	glm::mat4 tranform;

	double _old_time; // ����� ��������� ���������
	GLFWwindow* _window; // ������� ��������
	std::pair<int, int> _cell; // ���������� ������� ������

	std::shared_ptr<ShaderProgram> _shader;  // �������
	Map _map; // ����� ���������
	MeshCollection _meshes; // ��� ������������ � ������ � ���� ����
	MyOrbitCameraMover _orbit_camera; // ��� ������
	MyFreeCameraMover _free_camera;
	CameraMover* _current_camera;
};

