#pragma once

// ����� ������������ ���� 
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "glm/gtx/intersect.hpp"
#include "glm/gtx/transform.hpp"
#include <GLFW/glfw3.h>
#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"
#include "ShaderProgram.hpp"
#include <memory>
#include <fstream>
#include <iostream>
#include <vector>
#include <exception>
#include <utility>

struct Triangle {
	glm::vec3 a, b, c;
	glm::vec2 ta, tb, tc;
};

using Triangles = std::vector<Triangle>;

#define NEAR 0.03f
#define MESH_NUMBER 3 + POSTER_NUMBER
#define POSTER_NUMBER 5

#define DIR "7910LyamkinData2/"

#define UnBindPoint 0  // ����������� � ������ (� ��� ����� ���� �����)

GLuint getUniformBuffer(GLuint program); // ������� ����� ��� ������� ����������

void initGLFW();
void initGLEW();

void closeAll();

glm::vec3 normal(glm::vec3 a, glm::vec3 b, glm::vec3 c); // ������ ������� � ������������
float distanceToTriangle(glm::vec3 o, Triangle t); // ���������� �� ����� �� ������������

glm::vec3 project(const glm::vec3& v, const glm::mat4x4& m);

Triangles loadFromFile(std::string filename);

void joinTriangles(Triangles& t1, Triangles&& t2);

void projectTriangles(Triangles& t, glm::mat4x4 transform);