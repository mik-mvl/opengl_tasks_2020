/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330

uniform mat4 transform;

struct LightInfo
{
    vec3 pos; // обозначает положение или направление света в зависимости от point_flag;
    int point_flag; // вычислять для точечного источника (1) или направленного (0)
    vec3 La, Ld, Ls; 
};

uniform LightInfo light_info;
uniform int with_normal_map;

mat3 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s, 
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c);
}

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 textureCoords;

out vec2 tex_coords;
out vec3 normal;
out vec3 position;
out mat3 normal_rotate;

void main()
{
    gl_Position = transform * vec4(vertexPosition, 1.0);
    normal = vertexNormal;
    position = vertexPosition;
    tex_coords = textureCoords;
    if (with_normal_map == 1){
        vec3 Oz = vec3(0, 0, 1);
        if (length(Oz - normal) < 0.001){
            normal_rotate = mat3(1, 0, 0, 0, 1, 0, 0, 0, 1);
        } else {
            vec3 ax = normalize(cross(Oz, normal));
            float angle = acos(dot(Oz, normal));
            normal_rotate = rotationMatrix(ax, -angle);
        }
    }
}
