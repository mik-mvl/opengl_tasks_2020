#pragma once

#include "utils.h"
#include "mesh.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <set>
#include <tuple>
#include <algorithm>
#include <random>

struct Cell {
	int poster_side; // -1 -- ��� �������, ����� ����� �������� ������� � ��������
	size_t poster_type; // ����� �������
	bool pass[6]; // ����� ������� ���������
};

enum MeshTypes {
	Floor,
	Walls,
	Ceiling,
	Posters
};

class Map
{
public:
	Map();
	bool load(const std::string& filename);
	void save(const std::string& filename) const;
	void setSize(size_t h, size_t w);
	void randomize();
	bool initialized() const;
	bool& ceil();
	std::pair<int, int> getCell(glm::vec3 pos);
	bool canGo(glm::vec3 start, glm::vec3 finish);

	void triangles(MeshCollection& meshes) const;
	void triangles(int i, int j, int radius, MeshCollection& meshes) const;

private:
	static const int _dx[6], _dy[6];
	static const float _wall_thick;
	static const glm::vec3 v1, v2, v3;
	
	void _randomWalk(int i, int j);
	bool _inMap(int i, int j) const;

	Triangles _obstacles(int i, int j) const;
	void _cellTriangles(int i, int j, MeshCollection& meshes) const;
	void _cellPoster(int i, int j, MeshCollection& meshes) const;
	Triangles _floor, _ceiling, _door, _wall;
	size_t _h = 0, _w = 0;
	bool _init = false;
	bool _ceil = true;
	std::vector<std::vector<Cell>> _cells;
};

