#include "application.h"

Application::Application()
{
	_initAll();
}

Application::~Application()
{
	closeAll();
}

void Application::run()
{
    // ������� ���� ����������
	while (!glfwWindowShouldClose(_window)) { 
        glfwPollEvents();
        

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        _free_camera.updateScreen(width, height);
        auto camera_info = _current_camera->cameraInfo();

        _update();
        ImGui_ImplGlfwGL3_NewFrame();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // �������
        _shader->use(); // ���������� ������

        glm::mat4 transform = camera_info.projMatrix * camera_info.viewMatrix;
        _shader->setMat4Uniform("transform", transform);
        _shader->setVec3Uniform("camera_pos", _current_camera->position());
        _setLight();
        _meshes.draw(_shader);
        ImGui::Render();
        glfwSwapBuffers(_window);
	}
}

// ����������� �������. �������� ����������� ����������
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleKey(key, scancode, action, mods);
}

void windowSizeChangedCallback(GLFWwindow* window, int width, int height)
{
    // do nothing
}

void mouseButtonPressedCallback(GLFWwindow* window, int button, int action, int mods)
{
    // do nothing
}

void mouseCursosPosCallback(GLFWwindow* window, double xpos, double ypos)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleMouseMove(xpos, ypos);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleScroll(xoffset, yoffset);
}

void Application::_make_window()
{
    auto monitor_size = monitorSize();
    _window = glfwCreateWindow(monitor_size.first, monitor_size.second, "Beehive", nullptr, nullptr); // ������� ����, �� ����� ������� ������
    glfwMakeContextCurrent(_window); //������ ��� ���� �������

    if (!_window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }
    ImGui_ImplGlfwGL3_Init(_window, false);
    _setCallback();
}

void Application::_changeCamera()
{   // ����� ������� ������ ��� ������� 'c'
    if (_current_camera == &_free_camera) {
        _map.ceil() = false;
        _current_camera = &_orbit_camera;
        _map.triangles(_meshes); // whole map
    }
    else {
        _map.ceil() = true;
        _current_camera = &_free_camera;
        _updateMesh();
    }
}



void Application::_initAll()
{  //��������� �������������
    initGLFW();

    _make_window();

    initGLEW();

    _makeScene();

    _shader = std::make_shared<ShaderProgram>(DIR "shader.vert", DIR "shader.frag");

}

void Application::_initMeshes()
{
    _meshes.init(MESH_NUMBER);
    _meshes.index(MeshTypes::Floor).attachTexture(DIR "yellow_brick.png", 2);
    _meshes.index(MeshTypes::Floor).attachNMap(DIR "yellow_brick_normal.png");
    _meshes.index(MeshTypes::Walls).attachTexture(DIR "honeycomb.png", 1);
    _meshes.index(MeshTypes::Walls).attachNMap(DIR "honeycomb_normal.png");
    for (size_t i = 0; i < POSTER_NUMBER; ++i) {
        _meshes.index(MeshTypes::Posters + i).attachTexture(std::string(DIR "poster") + std::to_string(i) + std::string(".png"), 1);
        _meshes.index(MeshTypes::Posters + i).setMaterialInfo(glm::vec3(0.2, 0.2, 0.1), glm::vec3(0.7, 0.7, 0.3), glm::vec3(0.1, 0.1, 0.1), 100);
    }
}

void Application::_setCallback()
{   // ����������� �������
    glfwSetWindowUserPointer(_window, this); // ��������� �� ������� ����������
    glfwSetKeyCallback(_window, keyCallback);
    glfwSetWindowSizeCallback(_window, windowSizeChangedCallback);
    glfwSetMouseButtonCallback(_window, mouseButtonPressedCallback);
    glfwSetCursorPosCallback(_window, mouseCursosPosCallback);
    glfwSetScrollCallback(_window, scrollCallback);
}

void Application::_makeScene()
{   // �������� ��������� � �������� ����
    _initMeshes();
    _current_camera = &_free_camera;
    _free_camera.setMap(&_map);
    _map.setSize(10, 10);
    _map.randomize();
    _cell = std::make_pair(-1, -1);
    _updateMesh();
}


void Application::_update()
{
    // ���������� ������ � ����
    double cur_time = glfwGetTime();
    _current_camera->update(_window, cur_time - _old_time);
    _old_time = cur_time;
    _updateMesh();
}

void Application::_updateMesh()
{   // ��������� ������ ���, ���� ������������� � ������ ������
    if (_current_camera == &_free_camera) {
        auto cell = _map.getCell(_free_camera.position());
        if (cell != _cell) {
            _cell = cell;
            _map.triangles(_cell.first, _cell.second, 5, _meshes);
        }
    }
    
}

void Application::handleKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(_window, GL_TRUE);
    }
    if (action == GLFW_PRESS && key == GLFW_KEY_C) {
        _changeCamera();
    }
    _current_camera->handleKey(_window, key, scancode, action, mods);
}

void Application::handleMouseMove(double xpos, double ypos)
{
    _current_camera->handleMouseMove(_window, xpos, ypos);
}

void Application::handleScroll(double xoffset, double yoffset)
{
    _current_camera->handleScroll(_window, xoffset, yoffset);
}

void Application::_setLight()
{
    if (_current_camera == &_free_camera)
        _setLightInfo(_free_camera.position(), 1, glm::vec3(0.2, 0.2, 0.2), glm::vec3(0.8, 0.8, 0.8), glm::vec3(1, 1, 1));
    else
        _setLightInfo(glm::vec3(0.4, 0.4, -1), 0, glm::vec3(0.5, 0.5, 0.5), glm::vec3(0.9, 0.9, 0.9), glm::vec3(1, 1, 1));
}

void Application::_setLightInfo(glm::vec3 pos, bool point, glm::vec3 La, glm::vec3 Ld, glm::vec3 Ls)
{
    _shader->setVec3Uniform("light_info.pos", pos);
    _shader->setIntUniform("light_info.point_flag", int(point));
    _shader->setVec3Uniform("light_info.La", La);
    _shader->setVec3Uniform("light_info.Ld", Ld);
    _shader->setVec3Uniform("light_info.Ls", Ls);
}

std::pair<int, int> monitorSize()
{
    int monitorsCount;
    GLFWmonitor** monitors = glfwGetMonitors(&monitorsCount);
    const GLFWvidmode* videoMode = glfwGetVideoMode(monitors[monitorsCount - 1]);
    return std::pair<int, int>(videoMode->height, videoMode->width);
}
