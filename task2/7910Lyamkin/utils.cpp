#include "utils.h"

GLuint getUniformBuffer(GLuint program) {
	GLuint buffer;

	// �� �� ���������� ������ ������ � ��������� (DSA)
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_UNIFORM_BUFFER, buffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0); // ������ -- ���������

	glBindBufferBase(GL_UNIFORM_BUFFER, UnBindPoint, buffer); // ����������� �����
	return buffer;
}

void initGLFW()
{
	if (!glfwInit()) //�������������� ���������� GLFW
	{
		std::cerr << "ERROR: could not start GLFW3\n";
		exit(1);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // GLFW 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // ������������� � ������ ��������
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // ��� ������ ������������� �� ����
	// glfwSwapInterval(0); //��������� ������������ �������������
}


void initGLEW() {

	glewExperimental = GL_TRUE; // ��� ������ GLEW 1.13 � �����
	if (glewInit() != GLEW_OK) {
		std::cerr << "ERROR: could not start GLEW\n";
		exit(1);
	}; //�������������� ���������� GLEW

	glGetError(); // ������� ���� ������.

	/*
	DebugOutput _debug_output; // ���������� �����

	if (DebugOutput::isSupported())
		_debug_output.attach(); // ���������� �����-�����
	*/

	glEnable(GL_DEPTH_TEST); // �������� ���� �������
	glDepthFunc(GL_LESS); 

}


void closeAll()
{
	glfwTerminate();
	ImGui_ImplGlfwGL3_Shutdown();
}

glm::vec3 normal(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	glm::vec3 n = glm::cross(b - a, a - c);
	return glm::normalize(n);
}

float distanceToTriangle(glm::vec3 o, Triangle t)
{
	glm::vec3 a = t.a, b = t.b, c = t.c;
	glm::vec2 coef = glm::inverse(glm::mat2x2(glm::dot(b - a, b - a), glm::dot(c - a, b - a), glm::dot(b - a, c - a), 
		glm::dot(c - a, c - a))) * glm::vec2(glm::dot(o - a, b - a), glm::dot(o - a, c - a));
	float al = coef.x, be = coef.y;
	if (0 <= al && 0 <= be && al + be <= 1) { // �������� ������ ������ ������������
		return glm::distance(o, a + al * (b - a) + be * (c - a));
	}
	return std::min(glm::distance(o, a), std::min(glm::distance(o, b), glm::distance(o, c)));
}

glm::vec3 project(const glm::vec3& v, const glm::mat4x4& m) {
	auto mul = m * glm::vec4(v, 1);
	return glm::vec3(mul.x, mul.y, mul.z);
}

Triangles loadFromFile(std::string filename)
{
	Triangles triangles;
	std::ifstream in = std::ifstream(filename);
	glm::vec3 a, b, c;
	glm::vec2 ta, tb, tc;
	Triangle t;
	while (in >> a.x) {
		in >> a.y >> a.z >> ta.x >> ta.y >> b.x >> b.y >> b.z >> tb.x >> tb.y >> c.x >> c.y >> c.z >> tc.x >> tc.y;
		t.a = a, t.b = b, t.c = c, t.ta = ta, t.tb = tb, t.tc = tc;
		triangles.push_back(t);
	}
	return triangles;
}

void joinTriangles(Triangles& t1, Triangles&& t2)
{
	size_t os = t1.size();
	t1.resize(os + t2.size());
	std::copy(t2.begin(), t2.end(), t1.begin() + os);
	t2.clear();
}

void projectTriangles(Triangles& t, glm::mat4x4 transform)
{
	Triangles old = t;
	for (size_t i = 0; i < t.size(); ++i) {
		t[i].a = ::project(old[i].a, transform);
		t[i].b = ::project(old[i].b, transform);
		t[i].c = ::project(old[i].c, transform);
	}
}
