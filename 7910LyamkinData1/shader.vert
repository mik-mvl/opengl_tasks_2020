/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330

layout(std140) uniform mat4 transform;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    color = vec4(vertexNormal * 0.5 + vec3(0.5, 0.5, 0.5), 1);

    gl_Position = transform * vec4(vertexPosition, 1.0);
}
