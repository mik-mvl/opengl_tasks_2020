#pragma once
#include <iostream>

#include "utils.h"
#include "MyCameraMover.h"
#include "map.h"
#include "drawer.h"

std::pair<int, int> monitorSize();

class Application
{
public:
	Application();
	~Application();

	void run();

	void handleKey(int key, int scancode, int action, int mods);
	void handleMouseMove(double xpos, double ypos);
	void handleScroll(double xoffset, double yoffset);
private:
	void _changeCamera();
	void _initAll();
	void _setCallback(); // ��������� �������: �������� ������ ��� ��� ������������ ������
	void _makeScene();
	void _update();
	void _updateMesh();



	glm::mat4 tranform;

	double _old_time; // ����� ��������� ���������
	GLFWwindow* _window; // ������� ��������
	std::pair<int, int> _cell; // ���������� ������� ������

	Map _map; // ����� ���������
	std::shared_ptr<MeshDrawer> _drawer; // ����������� �������������
	MyOrbitCameraMover _orbit_camera; // ��� ������
	MyFreeCameraMover _free_camera;
	CameraMover* _current_camera;
};

