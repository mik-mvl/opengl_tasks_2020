#include "drawer.h"

MeshDrawer::MeshDrawer() : _shader("7910LyamkinData1/shader.vert", "7910LyamkinData1/shader.frag") {
	_buffer = getUniformBuffer(_shader.id());
}

MeshDrawer::~MeshDrawer()
{
}

void MeshDrawer::updateUniverse(std::vector<Triangle>&& triangles)
{
	std::vector<glm::vec3> data;
	for (auto t : triangles) {
		auto a = std::get<0>(t), b = std::get<1>(t), c = std::get<2>(t);
		auto norm = normal(t);
		data.push_back(a);
		data.push_back(norm);
		data.push_back(b);
		data.push_back(norm);
		data.push_back(c);
		data.push_back(norm);
	}
	std::vector<float> float_data;
	for (glm::vec3 p : data) {
		float_data.push_back(p.x);
		float_data.push_back(p.y);
		float_data.push_back(p.z);
	}
	_vcount = float_data.size();
	// ����������������� �����! 
	GLuint buffer;
	glGenBuffers(1, &buffer);

	glBindBuffer(GL_ARRAY_BUFFER, buffer); // ������ ���� ����� �������

	
	glBufferData(GL_ARRAY_BUFFER, float_data.size()* sizeof(float), float_data.data(), GL_STATIC_DRAW); // �������� ���������� ������� � ����� �� ����������

	glGenVertexArrays(1, &_vao); //������� ������ VertexArrayObject ��� �������� �������� ������������� ������

	glBindVertexArray(_vao); //������ ���� ������ �������

	glBindBuffer(GL_ARRAY_BUFFER, buffer); //������ ����� � ������������ �������

	
	glEnableVertexAttribArray(0); //�������� 0� ��������� ������� - ����������

	
	glEnableVertexAttribArray(1); //�������� 1� ��������� ������� - �������

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), nullptr);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), reinterpret_cast<void*>(3 * sizeof(float)));

	glBindVertexArray(0); // ������ -- ��������� 0.0
}

void MeshDrawer::updateCamera(CameraInfo info)
{
	_camera_info = info;
}

void MeshDrawer::draw()
{
	glm::mat4 transform = _camera_info.projMatrix * _camera_info.viewMatrix;
	_shader.use(); // ���������� ������
	setUniform(_shader.id(), _buffer, transform, "transform");
	glBindVertexArray(_vao);
	glDrawArrays(GL_TRIANGLES, 0, _vcount);
	_shader.setMat4Uniform("transform", transform);
}
