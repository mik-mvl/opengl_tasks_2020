#pragma once

#include "utils.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <set>
#include <tuple>
#include <algorithm>
#include <random>

struct Cell {
	bool pass[6];
};



class Map
{
public:
	bool load(const std::string& filename);
	void save(const std::string& filename) const;
	void setSize(size_t h, size_t w);
	void randomize();
	bool initialized() const;
	bool& ceil();
	std::pair<int, int> getCell(glm::vec3 pos);
	bool canGo(glm::vec3 start, glm::vec3 finish);

	std::vector<Triangle> triangles() const;
	std::vector<Triangle> triangles(int i, int j, int radius) const;

private:
	static const int _dx[6], _dy[6];
	static const glm::vec3 v1, v2, v3;
	
	void _randomWalk(int i, int j);
	bool _inMap(int i, int j) const;
	Triangle _door(int i, int j, int dir);

	Mesh _cellTriangles(int i, int j) const;
	size_t _h = 0, _w = 0;
	bool _init = false;
	bool _ceil = true;
	std::vector<std::vector<Cell>> _cells;
	mutable std::set<std::tuple<size_t, size_t, size_t, size_t>> _viewed;
};

