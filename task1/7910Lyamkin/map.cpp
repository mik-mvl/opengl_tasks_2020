#include "map.h"

const int Map::_dx[6]{ -1, -1, 0, 1, 1, 0 }, Map::_dy[6]{ 0, 1, 1, 0, -1, -1 };

const glm::vec3 Map::v1(glm::sqrt(3), 0, 0), Map::v2(glm::sqrt(3) / 2, 1.5, 0), Map::v3(0, 0, 1);

bool Map::load(const std::string& filename)
{
    std::ifstream in(filename);
    if (!in)
        return false;
    in >> _h >> _w;
    char c;
    for (size_t i = 0; i < _h; ++i)
        for (size_t j = 0; j < _w; ++j)
            for (size_t k = 0; k < 6; ++k) {
                in >> c;
                _cells[i][j].pass[k] = (c == '1');
            }
    _init = true;
    return true;
}

void Map::save(const std::string& filename) const
{
    std::ofstream out(filename);
    out << _h << ' ' << _w << '\n';
    for (size_t i = 0; i < _h; ++i) {
        for (size_t j = 0; j < _w; ++j) {
            for (size_t k = 0; k < 6; ++k)
                out << (int)_cells[i][j].pass[k];
            //out << ' ';
        }
        out << '\n';
    }
}

void Map::setSize(size_t h, size_t w)
{
    _h = h;
    _w = w;
    _cells.resize(_h);
    for (size_t i = 0; i < _h; ++i) {
        _cells[i].resize(_w);
        for (size_t j = 0; j < _w; ++j)
            for (size_t k = 0; k < 6; ++k)
                _cells[i][j].pass[k] = false;
    }
}

void Map::randomize()
{
    std::random_device rd;
    _randomWalk(0, 0);
    for (int i = 0; i < glm::sqrt(_h * _w); ++i) {
        _randomWalk(rd() % _h, rd() % _w);
    }
    _init = true;
}

bool Map::initialized() const
{
    return _init;
}

bool& Map::ceil()
{
    return _ceil;
}

std::pair<int, int> Map::getCell(glm::vec3 pos) // ���������� ���� i, j, j -- x-����������, i -- y-����������
{
    float x = pos.x, y = pos.y;
    float i = y / 1.5f;
    float j = (x - i * glm::sqrt(3) / 2) / glm::sqrt(3); 
    
    int k = int(i + 0.5), l = int(j + 0.5); // ����� �� ����� ���������, ������� � �������� ������
    std::pair<int, int> ans = std::make_pair(k, l);
    float min_dist = glm::length(glm::vec3(x, y, 0) - v1 * float(l) - v2 * float(k));
    for (int u = 0; u < 6; ++u) {
        int k1 = k + _dy[u], l1 = l + _dx[u];
        if (!_inMap(k, l))
            continue;
        float dist = glm::length(glm::vec3(x, y, 0) - v1 * float(l1) - v2 * float(k1));
        if (dist < min_dist) {
            min_dist = dist;
            ans = std::make_pair(k1, l1);
        }
    }
    return ans;
}

bool Map::canGo(glm::vec3 start, glm::vec3 finish)
{
    // ���������, ����� �� ������ �� ����� start � finish
    // ��������: ����� ������� 1.1 * NEAR ������ finish �� ���������� �� ���� ����������� ������ �������� �������
    if (glm::length(finish - start) > NEAR) {
        return false; // ������� ���� ������ ������
    }
    auto cell = getCell(start);
    _viewed.clear();
    auto triangles = _cellTriangles(cell.first, cell.second);
    for (auto t : triangles) {
        if (distanceToTriangle(finish, t) < NEAR * 1.1f)
            return false;
    }
    return true;
}

std::vector<Triangle> Map::triangles() const
{
    std::vector<Triangle> ans;
    _viewed.clear();
    for (size_t i = 0; i < _h; ++i)
        for (size_t j = 0; j < _w; ++j) {
            auto t = _cellTriangles(i, j);
            std::copy(t.begin(), t.end(), std::back_inserter(ans));
        }
    return ans;
}

std::vector<Triangle> Map::triangles(int i, int j, int radius) const
{
    
    std::vector<Triangle> ans;
    _viewed.clear();
    for (int k = i - radius; k <= i + radius; ++k)
        for (int l = j - radius; l <= j + radius; ++l) {
            if (!_inMap(k, l))
                continue;
            auto t = _cellTriangles(k, l);
            std::copy(t.begin(), t.end(), std::back_inserter(ans));
        }
    return ans;
}

void Map::_randomWalk(int i, int j)
{
    std::random_device rd;
    for (int l = 0; l < glm::sqrt(_h * _w); ++l) {
        int i1, j1, k;
        do {
            k = rd() % 6;
            i1 = i + _dy[k], j1 = j + _dx[k];

        } while (!_inMap(i1, j1));
        _cells[i][j].pass[k] = true;
        _cells[i1][j1].pass[(k + 3) % 6] = true;
        i = i1, j = j1;
    }
}

bool Map::_inMap(int i, int j) const
{
    return i >= 0 && j >= 0 && i < _h && j < _w;
}

Triangle Map::_door(int i, int j, int dir)
{
    int i1 = i + _dy[dir], j1 = j + _dx[dir];
    glm::vec3 center = float(j) * v1 + float(i) * v2;
    glm::vec3 dv = float(_dx[dir]) * v1 + float(_dy[dir]) * v2;
    glm::vec3 dvp = glm::normalize(glm::vec3(dv.y, -dv.x, 0));
    auto a = center + 0.5f * (dv + dvp), b = center + 0.5f * (dv - dvp);
    auto d = 0.5f * (a + b) + v3;
    auto f = 2.0f / 3.0f * a + 1.0f / 3.0f * b;
    auto g = 1.0f / 3.0f * a + 2.0f / 3.0f * b;
    return Triangle(f, d, g);
}

Mesh Map::_cellTriangles(int i, int j) const
{
    Mesh ans;
    glm::vec3 center = float(j) * v1 + float(i) * v2;
    for (int k = 0; k < 6; ++k) {
        int i1 = i + _dy[k], j1 = j + _dx[k];
        glm::vec3 dv = float(_dx[k]) * v1 + float(_dy[k]) * v2;
        glm::vec3 dvp = glm::normalize(glm::vec3(dv.y, -dv.x, 0));
        auto a = center + 0.5f * (dv + dvp), b = center + 0.5f * (dv - dvp);
        ans.push_back(Triangle(center, a, b)); // ���
        if (_ceil) {
            ans.push_back(Triangle(center + v3, a + v3, b + v3)); // �������
        }
        auto t1 = std::tuple<int, int, int, int>(i1, j1, i, j), t2 = std::tuple<int, int, int, int> (i, j, i1, j1); // ����� ������ �� �������� ���� �����
        if (_viewed.count(t1) > 0 || _viewed.count(t2) > 0) {
            continue;
        }
        _viewed.insert(t1);

        auto c = a + v3;
        auto e = b + v3;
        auto d = 0.5f * (c + e);
        auto f = 2.0f / 3.0f * a + 1.0f / 3.0f * b;
        auto g = 1.0f / 3.0f * a + 2.0f / 3.0f * b;
        ans.push_back(Triangle(a, f, c)); // �����
        ans.push_back(Triangle(c, d, f));
        ans.push_back(Triangle(d, e, g));
        ans.push_back(Triangle(e, g, b));
        if (!_cells[i][j].pass[k])
            ans.push_back(Triangle(f, d, g)); // = door(i, j, k) -- �������� ������ 
    }
    return ans;
}
