#include "application.h"

Application::Application()
{
	_initAll();
}

Application::~Application()
{
	closeAll();
}

void Application::run()
{
    // ������� ���� ����������
	while (!glfwWindowShouldClose(_window)) { 
        glfwPollEvents();
        

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        _free_camera.updateScreen(width, height);
        _drawer->updateCamera(_current_camera->cameraInfo());

        _update();
        ImGui_ImplGlfwGL3_NewFrame();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // �������

        _drawer->draw();
        ImGui::Render();
        glfwSwapBuffers(_window);
	}
}

// ����������� �������. �������� ����������� ����������
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleKey(key, scancode, action, mods);
}

void windowSizeChangedCallback(GLFWwindow* window, int width, int height)
{
    // do nothing
}

void mouseButtonPressedCallback(GLFWwindow* window, int button, int action, int mods)
{
    // do nothing
}

void mouseCursosPosCallback(GLFWwindow* window, double xpos, double ypos)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleMouseMove(xpos, ypos);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleScroll(xoffset, yoffset);
}

void Application::_changeCamera()
{   // ����� ������� ������ ��� ������� 'c'
    if (_current_camera == &_free_camera) {
        _map.ceil() = false;
        _current_camera = &_orbit_camera;
        auto mesh = _map.triangles(); // whole map
        _drawer->updateUniverse(std::move(mesh));
    }
    else {
        _map.ceil() = true;
        _current_camera = &_free_camera;
        _updateMesh();
    }
}

void Application::_initAll()
{  //��������� �������������
    initGLFW();

    auto monitor_size = monitorSize();
    _window = glfwCreateWindow(monitor_size.first, monitor_size.second, "Beehive", nullptr, nullptr); // ������� ����, �� ����� ������� ������
    glfwMakeContextCurrent(_window); //������ ��� ���� �������

    if (!_window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    initGLEW();
    
    
    ImGui_ImplGlfwGL3_Init(_window, false);
    
    _setCallback();

    _drawer = std::make_shared<MeshDrawer>();

    _makeScene();
}

void Application::_setCallback()
{   // ����������� �������
    glfwSetWindowUserPointer(_window, this); // ��������� �� ������� ����������
    glfwSetKeyCallback(_window, keyCallback);
    glfwSetWindowSizeCallback(_window, windowSizeChangedCallback);
    glfwSetMouseButtonCallback(_window, mouseButtonPressedCallback);
    glfwSetCursorPosCallback(_window, mouseCursosPosCallback);
    glfwSetScrollCallback(_window, scrollCallback);
}

void Application::_makeScene()
{   // �������� ��������� � �������� ����
    _current_camera = &_free_camera;
    _free_camera.setMap(&_map);
    _map.setSize(10, 10);
    _map.randomize();
    _cell = std::make_pair(-1, -1);
    _updateMesh();
}


void Application::_update()
{
    // ���������� ������ � ����
    double cur_time = glfwGetTime();
    _current_camera->update(_window, cur_time - _old_time);
    _old_time = cur_time;
    _updateMesh();
}

void Application::_updateMesh()
{   // ��������� ������ ���, ���� ������������� � ������ ������
    if (_current_camera == &_free_camera) {
        auto cell = _map.getCell(_free_camera.position());
        if (cell != _cell) {
            _cell = cell;
            auto mesh = _map.triangles(_cell.first, _cell.second, 5);
            _drawer->updateUniverse(std::move(mesh));
        }
    }
    
}

void Application::handleKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(_window, GL_TRUE);
    }
    if (action == GLFW_PRESS && key == GLFW_KEY_C) {
        _changeCamera();
    }
    _current_camera->handleKey(_window, key, scancode, action, mods);
}

void Application::handleMouseMove(double xpos, double ypos)
{
    _current_camera->handleMouseMove(_window, xpos, ypos);
}

void Application::handleScroll(double xoffset, double yoffset)
{
    _current_camera->handleScroll(_window, xoffset, yoffset);
}

std::pair<int, int> monitorSize()
{
    int monitorsCount;
    GLFWmonitor** monitors = glfwGetMonitors(&monitorsCount);
    const GLFWvidmode* videoMode = glfwGetVideoMode(monitors[monitorsCount - 1]);
    return std::pair<int, int>(videoMode->height, videoMode->width);
}
