#include "utils.h"

GLuint getUniformBuffer(GLuint program) {
	GLuint buffer;

	// �� �� ���������� ������ ������ � ��������� (DSA)
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_UNIFORM_BUFFER, buffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0); // ������ -- ��������� 0.0

	glBindBufferBase(GL_UNIFORM_BUFFER, UnBindPoint, buffer); // ����������� �����
	return buffer;
}

DebugOutput _debug_output; // ���������� �����

void initGLFW()
{
	if (!glfwInit()) //�������������� ���������� GLFW
	{
		std::cerr << "ERROR: could not start GLFW3\n";
		exit(1);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // GLFW 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // ������������� � ������ ��������
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // ��� ������ ������������� �� ����
	glfwSwapInterval(0); //��������� ������������ �������������
}


void initGLEW() {

	glewExperimental = GL_TRUE; // ��� ������ GLEW 1.13 � �����
	if (glewInit() != GLEW_OK) {
		std::cerr << "ERROR: could not start GLEW\n";
		exit(1);
	}; //�������������� ���������� GLEW

	glGetError(); // ������� ���� ������.
	DebugOutput _debug_output; // ���������� �����

	if (DebugOutput::isSupported())
		_debug_output.attach(); // ���������� �����-�����

	glEnable(GL_DEPTH_TEST); // ����� ����������� ������� ������� �������
	glDepthFunc(GL_LESS);  // �����������, ���� ������ ������ �������� �������

}


void closeAll()
{
	glfwTerminate();
	ImGui_ImplGlfwGL3_Shutdown();
}

glm::vec3 normal(Triangle t)
{
	glm::vec3 n = glm::cross(std::get<1>(t) - std::get<0>(t), std::get<0>(t) - std::get<2>(t));
	return glm::normalize(n);
}

float distanceToTriangle(glm::vec3 o, Triangle t)
{
	glm::vec3 a = std::get<0>(t), b = std::get<1>(t), c = std::get<2>(t);
	glm::vec2 coef = glm::inverse(glm::mat2x2(glm::dot(b - a, b - a), glm::dot(c - a, b - a), glm::dot(b - a, c - a), 
		glm::dot(c - a, c - a))) * glm::vec2(glm::dot(o - a, b - a), glm::dot(o - a, c - a));
	float al = coef.x, be = coef.y;
	if (0 <= al && 0 <= be && al + be <= 1) { // �������� ������ ������ ������������
		return glm::distance(o, a + al * (b - a) + be * (c - a));
	}
	return std::min(glm::distance(o, a), std::min(glm::distance(o, b), glm::distance(o, c)));
}
