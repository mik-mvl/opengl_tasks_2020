#pragma once
#include "utils.h"
#include "Camera.hpp"
#include "map.h"

// ������ � ����������� ��������, ������� ������������

/*
��������� ������ �������� � ����������� �����������. ������ ������ ������� � ����� �����.
*/
class MyOrbitCameraMover : public CameraMover
{
public:
    virtual void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    virtual void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    virtual void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    virtual void update(GLFWwindow* window, double dt) override;
    
protected:
    //��������� ����������� ������ �������� � ����������� ���������
    glm::vec3 _pos = glm::vec3(5.0f, 5.0f, 2.0f); // ������� ������
    double _phiAng = 1.6;   // pi / 2
    double _thetaAng = 0.0;
    double _r = 5.0;

    //��������� ������� ���� �� ���������� �����
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;
};

/*
������ ������ ����� ������
*/
class MyFreeCameraMover : public CameraMover
{
public:
    MyFreeCameraMover();

    virtual void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    virtual void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    virtual void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    virtual void update(GLFWwindow* window, double dt) override;

    void updateScreen(int w, int h);

    glm::vec3 position() const;
    void setMap(Map* map);
protected:
    void _updateMatrices();
    glm::vec3 _pos;
    glm::quat _rot;
    Map* _map; // ����� ���������
    int _screenw, _screenh;

    //��������� ������� ���� �� ���������� �����
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;
};
