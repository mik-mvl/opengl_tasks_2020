#pragma once

// ����� ������������ ���� 
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "glm/gtx/intersect.hpp"
#include <GLFW/glfw3.h>
#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"
#include "DebugOutput.h"
#include "ShaderProgram.hpp"
#include <memory>
#include <iostream>
#include <vector>
#include <exception>
#include <utility>

using Triangle = std::tuple<glm::vec3, glm::vec3, glm::vec3>;
using Mesh = std::vector<Triangle>;

#define NEAR 0.03f

#define UnBindPoint 0  // ����������� � ������ (� ��� ����� ���� �����)

GLuint getUniformBuffer(GLuint program); // ������� ����� ��� ������� ����������

void initGLFW();
void initGLEW();

void closeAll();

glm::vec3 normal(Triangle t); // ������ ������� � ������������
float distanceToTriangle(glm::vec3 o, Triangle t); // ���������� �� ����� �� ������������

template <typename T>
void setUniform(GLuint program, GLuint buffer, T value, const char name[]) { // ������������� �������� ������� ����������
	GLuint index[1];

	glGetUniformIndices(program, 1, &name, index);

	glBindBuffer(GL_UNIFORM_BUFFER, buffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(T), &value);
	
}