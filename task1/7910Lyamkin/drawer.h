#pragma once

#include <iostream>
#include <vector>
#include "utils.h"
#include "Camera.hpp"

class MeshDrawer
{
public:
	MeshDrawer();
	~MeshDrawer();
	void updateUniverse(std::vector<Triangle>&& triangles);
	void updateCamera(CameraInfo info);
	void draw();
private:
	int _vcount = 0;
	GLuint _vao = 0;
	GLuint _buffer;
	ShaderProgram _shader;
	std::vector<Triangle> _universe;
	CameraInfo _camera_info;
};

